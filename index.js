#!/usr/bin/env node
'use strict';

const program = require('commander');
const mongodb = require('mongodb');
const mongoUri = require('mongodb-uri');
const glob = require('glob');
const fs = require('fs');

program
  .version('0.0.1')
  .option('-d, --dir [required]', 'directory with json populate files (default ./resources)')
  .option('-u, --uri <required>', 'mongodb uri [default MONGO_URI e DATABASE_URL]')
  .option('-e, --only-erase','option description')
  .option('-p, --only-populate','we can have as many options as we want')
  .parse(process.argv); // end with parse to parse through the input

var mongoUrl = program.uri || process.env.MONGO_URL || process.env.DATABASE_URL;

if (!program['only-populate']) {
  excluirDb(() => {
    if (!program['only-erase']) {
      populateDb(() => process.exit())
    }
  })
} else {
  populateDb(() => process.exit());
}

function excluirDb(callback) {
  console.log(`Excluindo dados do banco agora: ${mongoUrl}`);  
  mongodb.connect(mongoUrl).then((mdb) => {
    const promiseDropArray = [];
    const parsedUri = mongoUri.parse(mongoUrl);
    const db = mdb.db(parsedUri.database);
  
    db.listCollections().toArray().then((collections) => {
      for (let i = 0; i < collections.length; i += 1) {
        if (collections[i].name.indexOf('system') < 0) {
          promiseDropArray.push(db.dropCollection(collections[i].name));
        }
      }
  
      Promise.all(promiseDropArray).then((values) => {
        mdb.close(true);
        console.log('Exclusão concluída');
        callback();
      });
    });
  }).catch((err) => {
    throw err;
  });
}

function populateDb(callback) {
  const baseDir = `${process.cwd()}/${(program.dir || 'resources')}/`;
  console.log({ baseDir });

  console.log(`Populando dados do banco: ${mongoUrl}`);
  
  glob(`${baseDir}*.json`, (err, files) => {
  
    mongodb.connect(mongoUrl).then((mdb) => {
      const promiseArray = [];
      const parsedUri = mongoUri.parse(mongoUrl);
      const db = mdb.db(parsedUri.database);
  
      for (let index = 0; index < files.length; index += 1) {
        const element = files[index];
        let content = fs.readFileSync(element, 'utf8');
        content = content && content !== '' ? content : '[]';
        const regs = JSON.parse(content);
        if (regs && regs.length > 0) {
          for (let j = 0; j < regs.length; j += 1) {
            if (regs[j]._id) {
              regs[j]._id = new mongodb.ObjectID(regs[j]._id);
            }
  
            for (const key in regs[j]) {
              if (key.indexOf('Ref') > -1) {
                regs[j][key.replace('Ref', '')] = new mongodb.ObjectId(regs[j][key]);
                delete regs[j][key];
              }

              if (regs[j][key].$date) {
                regs[j][key] = new Date(regs[j][key].$date);
              }
            }
          }
  
          const collection = element.replace('.json', '').replace(baseDir, '');
          console.log({
            collection,
            regs
          });
          promiseArray.push(db.collection(element.replace('.json', '').replace(baseDir, '')).insertMany(regs));
        }
      }
  
      Promise.all(promiseArray).then((values) => {
        mdb.close(true);
        console.log('Dados inseridos');
        callback()
      });
    });
  });
}
